<?php
include_once 'psl-config.php';
 
function sec_session_start() {
    $session_name = 'sec_session_id';   // Attribue un nom de session
    $secure = SECURE;
    // Cette variable empêche Javascript d’accéder à l’id de session
    $httponly = true;
    // Force la session à n’utiliser que les cookies
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Récupère les paramètres actuels de cookies
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"], 
        $cookieParams["domain"], 
        $secure,
        $httponly);
    // Donne à la session le nom configuré plus haut
    session_name($session_name);
    session_start();            // Démarre la session PHP 
    session_regenerate_id();    // Génère une nouvelle session et efface la précédente
}

function login($username, $password, $mysqli) {
    // L’utilisation de déclarations empêche les injections SQL
    if ($stmt = $mysqli->prepare("SELECT username, password 
        FROM user
       WHERE user = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $username);  // Lie "$email" aux paramètres.
        $stmt->execute();    // Exécute la déclaration.
        $stmt->store_result();
 
        // Récupère les variables dans le résultat
        $stmt->bind_result($username, $db_password);
        $stmt->fetch();
 
        // Hashe le mot de passe avec le salt unique
        if ($stmt->num_rows == 1) {
            // Si l’utilisateur existe, le script vérifie qu’il n’est pas verrouillé
            // à cause d’essais de connexion trop répétés 
 
                if ($db_password == $password) {
                    // Le mot de passe est correct!
                    // Récupère la chaîne user-agent de l’utilisateur
                    $username = $_SERVER['HTTP_USER_AGENT'];
                    // Protection XSS car nous pourrions conserver cette valeur
                    $username = preg_replace("/[^0-9]+/", "", $username);
                    $_SESSION['username'] = $username;
                    // Protection XSS car nous pourrions conserver cette valeur
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            // L’utilisateur n’existe pas.
            return false;
        }
    }
}