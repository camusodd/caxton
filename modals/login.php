<div class="modal fade" id="loginModal" role="dialog">';

  <div class="modal-dialog">
  
    <?php

      echo '<form class="form-horizontal" action="index.php?section=' . $section_demande . '" method="post">';

    ?>
    
      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Connexion</h4>
        </div>

        <div class="modal-body">

          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Courriel :</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="email" name="email" placeholder="Courriel">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Mot de passe :</label>
            <div class="col-sm-10">          
              <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Mot de passe">
            </div>
          </div>

<!--           <div class="form-group">        
            <div id="modal-remember" class="col-sm-offset-2 col-sm-10">
              <div class="checkbox">
                <label><input type="checkbox"> Se souvenir de moi</label>
              </div>
            </div>
          </div> -->

<!--      Ajouter le code PHP pour gérer le login invalide --> 
          <?php
            if (isset($login) AND $login == false) {
              echo '<div class="col-sm-offset-2 col-sm-10"><p class="invalide">Nom d\'usager ou mot de passe invalide</p></div>';
            } 

          ?> 
        </div> <!-- div modal-body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
          <button type="submit" class="btn btn-primary" name="submit" value="login">Soumettre</button>
        </div>

      </div><!-- div modal-content -->
    
    </form>

  </div> <!-- div modal-dialog -->
</div> <!-- div modal -->