<div class="modal fade" id="signinModal" role="dialog">
  <div class="modal-dialog">
  
    <?php

      echo '<form class="form-horizontal" action="index.php?section=' . $section_demande . '" method="post">';

    ?>
    
      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Inscription</h4>
        </div>

        <div class="modal-body">

          <div class="form-group">
            <label class="control-label col-sm-3" for="prenom">Prénom :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-3" for="nom">Nom :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-3" for="email">Courriel :</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" id="email" name="email" placeholder="Courriel">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-3" for="pwd">Mot de passe :</label>
            <div class="col-sm-9">          
              <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Mot de passe">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-3" for="pwd2">Retapez le mot de passe :</label>
            <div class="col-sm-9">          
              <input type="password" class="form-control" id="pwd2" name="pwd2" placeholder="Retapez le mot de passe">
            </div>
          </div>

<!--      Ajouter le code PHP pour gérer les erreurs --> 
          <?php
            if (isset($signin) AND $signin == false) {
              echo '<div class="col-sm-offset-2 col-sm-10"><p class="invalide">' . $err . '</p></div>';
            } 

          ?> 
        </div> <!-- div modal-body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
          <button type="submit" class="btn btn-primary" name="submit" value="sigin">Soumettre</button>
        </div>

      </div><!-- div modal-content -->
    
    </form>

  </div> <!-- div modal-dialog -->
</div> <!-- div modal -->