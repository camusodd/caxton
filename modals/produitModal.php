<div class="modal fade" id="produitModal" role="dialog">
  <div class="modal-dialog">
    <?php

      echo '<form class="form-horizontal" action="index.php?section=' . $section_demande . '" method="post">';

    ?>
    
      <!-- Modal content-->
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ajout d'un produit</h4>
        </div>

        <div class="modal-body">

          <div class="form-group">
            <label class="control-label col-sm-3" for="nomProduit">Nom :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="nomProduit" name="nomProduit" placeholder="nom">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-3" for="prix">Prix :</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="prix" name="prix" placeholder="prix">
            </div>
          </div>


<!--      Ajouter le code PHP pour gérer les erreurs --> 
          <?php
            if (false == true) {
              echo '<div class="col-sm-offset-2 col-sm-10"><p class="invalide">' . $err . '</p></div>';
            } 

          ?> 
        </div> <!-- div modal-body -->

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
          <button type="submit" class="btn btn-primary" name="submit" value="ajoutProduit">Soumettre</button>
        </div>

      </div><!-- div modal-content -->
    
    </form>

  </div> <!-- div modal-dialog -->
</div> <!-- div modal -->