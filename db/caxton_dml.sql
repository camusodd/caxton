-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 10 Janvier 2017 à 15:51
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `caxton`
--
CREATE DATABASE IF NOT EXISTS `caxton` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `caxton`;

--
-- Vider la table avant d'insérer `ingredients`
--

TRUNCATE TABLE `ingredients`;
--
-- Contenu de la table `ingredients`
--

INSERT INTO `ingredients` (`id`, `nom_fr`) VALUES
(1, 'Farine du moulin d''à côté'),
(2, 'Bicarbonate de soude'),
(3, 'Sel de Guérande'),
(4, 'Beurre fermier de Monsieur Séguin'),
(5, 'Cassonade des îles'),
(6, 'Sucre'),
(7, 'Oeuf du poulailler du village'),
(8, 'Chocolat au lait raffiné'),
(9, 'Confiture de Madame Caxton'),
(10, 'Sucre en poudre léger'),
(11, 'Gingembre moulu'),
(12, 'Cannelle moulu'),
(13, 'Muscade moulu'),
(14, 'Cassonade antillaise'),
(15, 'Mélasse antillaise'),
(16, 'Vanille des îles'),
(17, 'Poudre à pâte');

--
-- Vider la table avant d'insérer `ingredients_produits`
--

TRUNCATE TABLE `ingredients_produits`;
--
-- Contenu de la table `ingredients_produits`
--

INSERT INTO `ingredients_produits` (`id_ingredient`, `id_produit`, `ordre`) VALUES
(1, 1, 0),
(1, 2, 0),
(1, 3, 0),
(1, 4, 0),
(2, 1, 1),
(2, 3, 2),
(3, 1, 2),
(3, 3, 6),
(4, 1, 3),
(4, 2, 3),
(4, 3, 5),
(4, 4, 3),
(5, 1, 4),
(6, 1, 5),
(6, 2, 1),
(6, 4, 1),
(7, 1, 6),
(7, 2, 6),
(7, 3, 9),
(8, 1, 7),
(9, 2, 4),
(10, 2, 5),
(10, 4, 5),
(11, 3, 1),
(12, 3, 3),
(13, 3, 4),
(14, 3, 7),
(15, 3, 8),
(16, 4, 4),
(17, 2, 2),
(17, 4, 0);

--
-- Vider la table avant d'insérer `pages`
--

TRUNCATE TABLE `pages`;
--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`nom`, `description`, `ordre`, `menu`) VALUES
('about', 'Qui nous sommes ?', 1, 1),
('contact', 'Se rendre à la boutique', 4, 1),
('details', 'Description détaillée d''un produit', 0, 0),
('home', 'Accueil', 0, 1),
('legendes', 'Quelques légendes', 2, 1),
('products', 'Nos biscuits', 3, 1);

--
-- Vider la table avant d'insérer `produits`
--

TRUNCATE TABLE `produits`;
--
-- Contenu de la table `produits`
--

INSERT INTO `produits` (`id`, `nom`, `prix`, `image`, `promotion`, `description_courte`, `lien_externe`, `description_longue`) VALUES
(1, 'Chasse-Galerie', 1.1, 'chocolate_chip.jpg', 1, 'biscuits aux brisures de chocolat', 'https://fr.wikisource.org/wiki/La_Chasse-galerie_(recueil)/La_Chasse-galerie', 'Pour le plus grand plaisir de vos papilles, il est composé de brisures de chocolat au lait rafiné qui vous réconforteront au plus froid de l''hiver. Nul besoin de partir en canot volant pour conquérir votre douce moitié. Offrez lui simplement une boîte de ces délicieux biscuits.'),
(2, 'Loup-Garou', 1.25, 'confiture.jpg', 0, 'biscuits aux confitures', 'https://fr.wikisource.org/wiki/La_Chasse-galerie_(recueil)/Le_Loup-garou', 'Ce biscuit tendre et savoureux vous transformera, mais n''ayez craindre, pleine lune ou pas, il ne vous poussera ni oreille, ni queue.'),
(3, 'Macloune', 1, 'ginger.jpg', 1, 'biscuits au pain d''épices', 'https://fr.wikisource.org/wiki/La_Chasse-galerie_(recueil)/Macloune', 'Ce biscuit en pain d''épices arrive juste à temps pour Noël. Tel que Macloune ce biscuit rempli de bonté saura vous ravir.'),
(4, 'Père Louis', 2, 'sables.jpg', 0, 'biscuits sablés', 'https://fr.wikisource.org/wiki/La_Chasse-galerie_(recueil)/Le_Père_Louison', 'C''est un biscuit sablé qui contrairement au Père Louison n''est pas du tout sec. Au contraire, il garde tout son moelleux avec sa fine texture granuleuse.');

--
-- Vider la table avant d'insérer `utilisateurs`
--

TRUNCATE TABLE `utilisateurs`;
--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`email`, `nom`, `prenom`, `password`, `administrator`) VALUES
('annema.burns@gmail.com', 'Burns', 'Anne-Marie', 'dd06b1bbbfb0665c145e83089dd87f9eb53f55fd', 0),
('lambj00@hotmail.com', 'Lambert', 'Jean-Francois', '002e23b9512c2b19a361d0fa9d7207e91c14c61a', 1),
('mlavoie@gmail.com', 'Lavoie', 'Mathieu', 'd926809a7aaa81ebb5542572d2e85107c50a1912', 0),
('utilisateur.caxton@gmail.com', 'Caxton', 'Utilisateur', 'e1dfa236a646647b9b5a7c15d5e2739a916b4dda', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
