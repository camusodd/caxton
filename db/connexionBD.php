<?php
	// Sous WAMP ET XAMMP(Windows)
	$hostname = "localhost";
	$dbname = "Caxton";
	$username = "root";
	$password = "";

	try {

		$bdd = new PDO('mysql:host=' . $hostname . 
			           ';dbname=' . $dbname .
			           ';charset=utf8', $username, $password,
		               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

				
	} catch(Exception $e) {
		die('Erreur : ' . $e->getMessage());
	}
			 
?>