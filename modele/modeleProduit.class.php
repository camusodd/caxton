<?php

	require_once 'framework/modele.php';

	class modeleProduit extends Modele {

		public function getProduits(){
		 
		    $sql = 'SELECT * FROM produits';
            $produit = $this->executerRequete($sql);

            if($produit->rowCount() > 0) {
            	return $produit->fetchALL(); 
            } else {
            	 throw new Exception('Aucun produit retourné.');
            }
		}
		
		public function getPromotion(){
		 
		    $sql = 'SELECT * FROM produits where promotion = true';
            $produits = $this->executerRequete($sql);

            if($produits->rowCount() > 0) {
            	return $produits->fetchALL(); 
            } else {
            	 throw new Exception('Aucun produit en promotion retourné par la requête.');
            }
		}
		public function insertProduct($nom, $prix){
			$sql = 'INSERT INTO produits (nom, prix) VALUES(:nom, :prix)';
			return $this->executerRequete($sql, array('nom' => $nom, 'prix' => $prix));
		}

		public function getPrix() {
			return $this->_prix;
		}


	}