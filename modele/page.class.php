<?php
	require_once 'framework/modele.php';

	class Page extends Modele {

		private $_nom;
		private $_description;
		private $_ordre;
		private $_menu;


		public function __construct($nom){

		    $sql = 'SELECT * FROM pages WHERE nom = :nom';
            $page = $this->executerRequete($sql, array('nom' => $nom));

            if($page->rowCount() > 0) {
                $array = $page->fetch();
                $this->_nom = $array['nom'];
            } else {
            	$this->_nom = "404";
            }
		}
	
		public function getNom() {
			return $this->_nom;
		}

	}