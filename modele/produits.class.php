<?php

	require_once 'framework/modele.php';

	class Produit extends Modele {

		private $_id;
		private $_nom;
		private $_prix;
		private $_image;
		private $_promotion;
		private $_description_courte;
		private $_lien_externe;
		private $_description_longue;


		public function __construct($nom){
		 
		    $sql = 'SELECT * FROM produits WHERE nom = :nom';
            $produit = $this->executerRequete($sql, array('nom' => $nom));

            if($produit->rowCount() > 0) {
                $array = $produit->fetch();
                $this->_id  = $array['id'];
                $this->_nom = $array['nom'];
                $this->_prix  = $array['prix'];

            } else {
            	$this->_prix = 10.10;
            }
		}

		public static function insertProduct($nom, $prix){
			$sql = 'INSERT INTO produits (nom, prix) VALUES(:nom, :prix)';
			$produit = $this->executerRequete($sql, array('nom' => $nom, 'prix' => $prix));
			echo $produit;
		}

		public function getPrix() {
			return $this->_prix;
		}


	}