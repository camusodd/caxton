<?php

	require_once 'framework/modele.php';

	class ModeleUtilisateur extends Modele {

		public function getUtilisateur($email, $pwd) {

		    $sql = 'SELECT * FROM utilisateurs WHERE email = :email and password = :pwd';
            $utilisateur = $this->executerRequete($sql, array('email' => $email, 'pwd' => sha1($pwd)));

            if($utilisateur->rowCount() > 0) {
                return $utilisateur->fetch();
            } else {
            	throw new Exception("Nom d'usager ou mot de passe invalide");
            }
		}

		public function enregistrerUtilisateur($prenom, $nom, $email, $pwd) {

		    $requete = 'INSERT INTO utilisateurs (nom, prenom, email, password) VALUES (:nom, :prenom, :email, :pwd)';
            return $this->executerRequete($requete, array('prenom' => htmlspecialchars($prenom), 'nom' => htmlspecialchars($nom), 'email' => $email, 'pwd' => sha1($pwd)));
 		}



	}