<main>
    <div class="container">
        <div class="row">
            <div id="contenu-principale" class="col-md-12">
		        <h1>Bienvenue sur le site des biscuits du comte de Caxton</h1>
		        <p>
		            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet ipsum magna, et convallis sem. Nunc pulvinar magna vitae orci malesuada blandit. Proin ac purus dui. Suspendisse venenatis egestas egestas. Sed tincidunt diam et massa convallis et mollis enim malesuada. Nullam eget metus nunc, eget imperdiet urna. Quisque vehicula ipsum non sapien vulputate convallis quis quis ligula. Aenean gravida mollis blandit. Nullam lacus tortor, viverra a auctor ac, porta eget neque. Duis placerat mi metus, a gravida quam.
		            <br /><br />
		            Nam ut augue mauris, at dapibus quam. Pellentesque eu nunc enim. Proin facilisis, dolor nec sollicitudin mattis, sem velit mollis sem, sagittis sagittis libero ante id nunc. Nam congue, mauris non porttitor convallis, purus elit faucibus nunc, in mollis sem lorem eu tortor. Cras nec justo id libero fringilla placerat ac et leo. Mauris ac vehicula odio. Cras augue erat, sodales vel porttitor ut, scelerisque nec justo. Praesent vitae lorem erat. Suspendisse elementum tortor vitae diam venenatis eget fermentum lacus suscipit. Quisque varius vulputate tempus.
		            ...
		        </p>
		    </div>
		</div>

        <div class="row">
        	<div id="contenu-principale" class="col-md-12">
        		<h2>En vedette cette semaine :</h2>
        	</div>
            <div id="contenu-principale" class="col-md-12 article-flex">


                <?php

                    $modProduit = new modeleProduit();
                    $prods = $modProduit->getPromotion();


                    include("article_produits.php");

                ?>

            </div>
        </div>
    </div>
</main>