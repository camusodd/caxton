<main>
    <div class="container">
        <div class="row">
            <div id="contenu-principale" class="col-md-12 article-flex">
                <article>
                    <img src="images/chassegalerie.jpg" alt="Canoë volant emportant les hommes" title="La chasse galerie" />
                    <p class="normal">
                        Pour lors que je vais vous raconter une rodeuse d’histoire, 
                        dans le fin fil ; mais s’il y a parmi vous-autres des lurons 
                        qui auraient envie de courir la chasse-galerie ou le loup-garou, 
                        je vous avertis qu’ils font mieux d’aller voir dehors si les chats-huants 
                        font le sabbat, car je vais commencer mon histoire en faisant un grand 
                        signe de croix pour chasser le diable et ses diablotins. J’en ai eu assez 
                        de ces maudits-là dans mon jeune temps.
                        <br />
                        <a href="https://fr.wikisource.org/wiki/La_Chasse-galerie_(recueil)/La_Chasse-galerie" title="La chasse galerie" target='_blank'>
                        Plus ... </a>    
                    </p>
                </article>
                <article>
                    <img src="images/loupgarou.jpg" alt="têtes d'hommes et de loups" title="Le loup garou" />
                    <p class="normal">
                    OUI ! Vous êtes tous des fins-fins, les avocats de Montréal, pour 
                    vous moquer des loups-garous. Il es vrai que le diable ne fait 
                    pas tant de cérémonies avec vous-autres et qu’il est si sûr de 
                    son affaire, qu’il n’a pas besoin de vous faire courir la 
                    pretentaine pour vous attraper par le chignon du cou, à l’heure 
                    qui lui conviendra.
                    <a href="https://fr.wikisource.org/wiki/La_Chasse-galerie_(recueil)/Le_Loup-garou" title="Le loup garou" target='_blank'>
                    Plus ... </a> 
                    </p>
                </article>
                <article>
                    <img src="images/louison.jpg" alt="Portrait du père Louison" title="Le père Louison" />
                    <p class="normal">
                    C’était un grand vieux, sec, droit comme une flèche, comme on 
                    dit au pays, au teint basané, et la tête et la figure couvertes 
                    d’une épaisse chevelure et d’une longue barbe poivre et sel.
                    </p>
                    <p>
                    Tous les villageois connaissaient le père Louison, et sa 
                    réputation s’étendait même aux paroisses voisines ; son métier 
                    de canotier et de passeur le mettait en relations avec tous les 
                    étrangers qui voulaient traverser le St. Laurent, large en cet 
                    endroit d’une bonne petite lieue.
                    </p>
                    <a href="https://fr.wikisource.org/wiki/La_Chasse-galerie_(recueil)/Le_Père_Louison" title="Le père Louison" target='_blank'>
                    Plus ... </a> 
                </article>
            </div>
        </div>
    </div>
</main>