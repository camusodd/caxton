<main>
    <div class="container">
        <div class="row">
            <div id="contenu-principale class="col-md-12">
                <h1>Les biscuits du comte de Caxton </h1>
                <p class="adresse">   
                    10 rue Saint-Louis <br />
                    Saint-Elie-de-Caxton, Québec <br />
                    G0X 2N0 
                </p>
                <p class="adresse">(819) 221-2839 </p>
                <p class="adresse"><a href="mailto:comtecaxton@biscuits.com" title="Nous envoyer un courriel">comtecaxton@biscuits.com</a> </p>
                <h2>Heures d’ouverture </h2>
                <table>
                    <tr>
                        <th>Dimanche</th>
                        <th>Lundi</th>
                        <th>Mardi</th>
                        <th>Mercredi</th>
                        <th>Jeudi</th>
                        <th>Vendredi</th>
                        <th>Samedi</th>
                    </tr>
                    <tr>
                        <td>Messe du village&nbsp; à 11h00</td>
                        <td colspan="5">8h00-12h00</td>
                        <td>On fait la grâce matinée !</td>
                    </tr>
                    <tr>
                        <td colspan="7">Venez nous trouver au bistrot d&#39;en face pour dîner !</td>
                    </tr>
                    <tr>
                        <td>13h30-17h00</td>
                        <td>13h30-19h00</td>
                        <td colspan="3">13h30-18h00</td>
                        <td>13h30-19h00</td>
                        <td>13h30-17h00</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</main>