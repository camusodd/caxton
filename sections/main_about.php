<main>
    <div class="container">
        <div class="row">
            <div id="contenu-principale" class="col-md-12">
                <h1>L’histoire d’une biscuiterie de village</h1>
                <img src="images/maison.jpg" alt="Maison de village en pierre" title="Photo de la boutique de biscuit" />
                <h2>La construction de la maison familiale</h2>
                <p class="normal">C&#39;est en 1895 que notre arrière-grand-père Arthur, scieur de bois au moulin 
                    du village, décida de construire la maison familiale. Après quelques années de 
                    construction, la demeure familiale pris pied près de l&#39;église paroissiale avec 
                    les quelques 30 autres habitations qui formaient alors le village. Suivi alors 
                    une traîné d&#39;enfants comme il était coutume d&#39;en voir dans les villages d&#39;antan. 
                    Des 8 enfants de la famille Rimbault, les 4 garçons s&#39;embauchèrent au moulin à 
                    scie et 3 des filles trouvèrent un époux auprès d&#39;agriculteurs et de bûcherons 
                    des villages voisins.</p>
                <h2>La tradition des biscuits</h2>
                <p class="normal">Marie, la plus jeune, épousa le boulanger et ce fut le début de la 
                    fabrication de biscuit dans la famille. Les biscuits de Marie attiraient les 
                    hommes des alentours fatigués par le travail manuel. Ces célèbres lutins bleus 
                    faisaient la renommé de la boulangerie. Très vite, ont accouru des villages 
                    voisins pour goûter ces délices. Marie se gardait bien de dévoiler ses recettes 
                    aux noms inspirés des légendes racontées le soir au coin du feu. Depuis lors, 
                    tenues secrètes, elles se transmettent de mère en filles et de filles en fils.</p>
                <h2>Tim, notre chef pâtissier</h2>
                <p class="normal">Le cousin Tim, petit-fils de Marie a raffiné la tradition ancestrale en 
                    allant étudier l&#39;art de la pâtisserie auprès des grands maîtres de toute 
                    l&#39;Europe. De retour au village, il prépare soigneusement les recettes de sa 
                    grand-mère en y ajoutant une touche de ce savoir-faire infusé d&#39;air d&#39;ailleurs. 
                    Ainso, nos biscuits de légendes sont un mélange de traditions et d&#39;aventures 
                    autour du globe.</p>
            </div>
        </div>
    </div>
</main>