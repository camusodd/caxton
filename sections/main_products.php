<main>
    <h1>Nos biscuits</h1>
    <div class="container">
        <div class="row">
            <div id="contenu-principale" class="col-md-12 article-flex">

                <?php

                    //$requete = $bdd->query('SELECT * FROM produits');
                    $modProduit = new modeleProduit();
                    $prods = $modProduit->getProduits();

                    include("article_produits.php");
                    if (isset($_SESSION['administrateur']) and $_SESSION['administrateur'] == true){
                        $admin_form = '<article class="produits"><h4>Administrateur</h4><button type="button" data-toggle="modal" data-target="#produitModal">Ajoutez un produit!</button></article>';
                        echo $admin_form;
                    }
                            

                ?>

            </div>
        </div>
    </div>
</main

<!-- DEBUT: modal d'ajout de produit -->
<?php include("modals/produitModal.php"); ?>
<!-- FIN: modal d'ajout de produit -->