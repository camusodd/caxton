

<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> 
                </button>
                <a class="navbar-brand" href="#">
                    <img class="large-logo" src="images/comte.png" />
                    <img class="small-logo" src="images/comte-small.png" />
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">

                <?php

                    $requete = $bdd->query('SELECT * FROM pages WHERE menu=true ORDER BY ordre');

                    $li_string = "";

                    while($donnees = $requete->fetch()) {
    
                        $li_string = $li_string . "<li ";

                        if($section_demande == $donnees['nom']) {
                            $li_string = $li_string . 'class="active"';
                        }    
                    
                        $li_string = $li_string . '><a href="index.php?section=' . $donnees['nom'] . '" title="' . $donnees['description'] . '">' . $donnees['description'] . '</a></li>';
                    }    

                    echo $li_string;
                ?>

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#signinModal" data-toggle="modal"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>

                    <?php

                        if (isset($_SESSION['usager'])) {
                            echo '<li><a href="index.php?section=' . $section_demande . '&logout=true"><span class="glyphicon glyphicon-log-out"></span> Déconnexion</a></li>';
                        } else {
                            echo '<li><a href="#loginModal" data-toggle="modal"><span class="glyphicon glyphicon-log-in"></span> Connexion</a></li>';
                        }
                    ?>

                </ul>
            </div>
        </div>
        <p class="user">

            <?php 
                if(isset($_SESSION['usager'])) {
                    echo $_SESSION['usager'];
                }
            ?>

        </p>
    </nav>
</header>