<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>&copy; Les biscuits du comte de Caxton. Tous droits réservés.</h1>
                <p>   
                    10 rue Saint-Louis,
                    Saint-Elie-de-Caxton, Québec, 
                    G0X 2N0 -
                    Tél. : (819) 221-2839
                </p>
            </div>
        </div>
    </div>
</footer>