<?php

    $requete = $bdd->prepare('SELECT * FROM produits WHERE Id = :idProduit');
    $requete->bindParam('idProduit', $_GET['id'], PDO::PARAM_INT);
    $requete->execute();

    if($requete->rowCount() > 0) {
        $donnees = $requete->fetch();
        $formatted = str_replace('.', ',', sprintf("%01.2f $", $donnees['prix']));
?>

        <main>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Le <?php echo $donnees['nom']; ?></h1>
                    </div>
                </div>
        		<div class="row">
        			<div class="col-md-6 col-vertical-center">
                        <p class="normal">Le <?php echo $donnees['nom']; ?> est un <?php echo $donnees['description_courte']; ?> inspiré de la 
                        <a href="<?php echo $donnees['lien_externe']; ?>" title="La <?php echo $donnees['nom']; ?>" target='_blank'>célèbre légende d'Honoré Beaugrand.</a> <?php echo $donnees['description_longue']; ?></p>
                        <h2 class="ingredients">Ingrédients</h2>
                        <ul class="ingredients">

                        <?php

                            //$requete = $bdd->prepare('SELECT * FROM Ingredients WHERE Id IN (SELECT id_ingredient FROM ingredients_produits WHERE id_produit = :idProduit ORDER BY ordre)');
                            $requete = $bdd->prepare('SELECT * FROM ingredients INNER JOIN ingredients_produits ON ingredients.id = ingredients_produits.id_ingredient WHERE id_produit = :idProduit ORDER BY ordre');
                            $requete->bindParam('idProduit', $_GET['id'], PDO::PARAM_INT);
                            $requete->execute();

                            $liste = "";

                            while($ingredients = $requete->fetch()) {
                                $liste = $liste . '<li>' . $ingredients['nom_fr'] .'</li>';

                            }

                            echo $liste;
                            

                        ?>
        				</ul>
                    </div><!--
                    --><div class="col-md-6 col-vertical-center">
                    	<figure>
                        	<img src="images/biscuits/<?php echo $donnees['image']; ?>" alt="<?php echo $donnees['description_courte']; ?>" title="Le <?php echo $donnees['nom']; ?>" />
                        	<figcaption>Le <?php echo $donnees['nom'] . ' ' . $formatted; ?></figcaption>
                        </figure>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <a href="index.php?section=products" title="Nos biscuits">Retour au catalogue des biscuits</a>
                    </div>
                </div>
            </div>
        </main>

<?php

    } else {
        
        include("sections/main_404.php");
    }
?>