<?php 
    session_start();

    include("db/connexionBD.php"); 
    include("modele/utilisateur.class.php");
    require("modele/page.class.php");
    require("modele/produits.class.php");
    require("modele/modeleProduit.class.php");


    if (isset($_GET['logout']) and $_GET['logout'] == "true") {
        session_destroy();
        $_SESSION['usager'] = null;
    }

    // vérifie si un formulaire a été envoyé avec la méthode post
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['submit'])) {

        // vérifie de quel formulaire il s'agit
        if($_POST['submit'] == "login") {
    
            if (isset($_POST['email']) and isset($_POST['pwd'])) {

                try {
                     $modUtilisateur = new ModeleUtilisateur();
                     $utilisateur = $modUtilisateur->getUtilisateur($_POST['email'], $_POST['pwd']);
                     $_SESSION['usager'] = $utilisateur['prenom'] . " " . $utilisateur['nom'];
                     $_SESSION['administrateur'] = $utilisateur['administrator'];
                     $login = true;
                 } catch(Exception $e) {
                     $login = false;
                 }

            }

        } elseif ($_POST['submit'] == "sigin") {
            // vérifie que les champs ne sont pas vide
            if (isset($_POST['prenom']) and !empty($_POST['prenom']) and 
                isset($_POST['nom']) and !empty($_POST['nom']) and 
                isset($_POST['email']) and !empty($_POST['email']) and 
                isset($_POST['pwd']) and !empty($_POST['pwd']) and 
                isset($_POST['pwd2']) and !empty($_POST['pwd2'])) {

                // vérifie que les mots de passe sont identiques
                // on pourrait aussi appliquer des règles pour vérifier que
                // les mots de passe sont assez complexes
                if($_POST['pwd'] == $_POST['pwd2']) {

                    // vérifie que l'adresse de courriel est d'un format valide
                    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

                        try {
                            $modUtilisateur = new ModeleUtilisateur();
                            $requete = $modUtilisateur->enregistrerUtilisateur($_POST['prenom'], $_POST['nom'], $_POST['email'], $_POST['pwd']);

                            if($requete){
                            //if($requete->rowCount() > 0) {
                                $_SESSION['usager'] = $_POST['prenom'] . " " . $_POST['nom'];
                                $signin = true;
                                $login = true;
                            } else {
                                $signin = false;  
                                $err = "Votre compte n'a pas pu être crée";
                            }
                        } catch(Exception $e) {
                            $signin = false; 
                            $err = "Un compte avec cette adresse de courriel existe déjà";
                        }

                    } else{
                        $signin = false;
                        $err = "L'adresse de courriel saisie est invalide";

                    }

                } else {
                    $signin = false;
                    $err = "Le mot de passe n'est pas identique";
                }

            } else {
                $signin = false;
                $err = 'Vous devez remplir tous les champs';
            }
        }elseif ($_POST['submit'] == "ajoutProduit"){
            try{
                $newProduct = new modeleProduit();
                $result = $newProduct->insertProduct($_POST['nomProduit'], $_POST['prix']);
            }catch(Exception $e) {
                 echo 'Message: ' .$e->getMessage();
            }
        }

    }

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Les biscuits Caxton</title>
        <meta charset="UTF-8">
        
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link rel="stylesheet" href="css/style.css" />

    </head>
    <body>

        <?php


            $section_demande = "";

            if (isset($_GET['section'])) {

                
                $page = new Page($_GET['section']);

                $section_demande = $page->getNom();


             } else {
                 $section_demande = "home";
             }

        ?>
        <!-- DEBUT: Entête fixe -->
        <?php include("sections/header.php"); ?>
        <!-- FIN: Entête fixe -->

        <!-- DEBUT: Partie principale de la page -->
        <?php include("sections/main_" . $section_demande . ".php"); ?>
        <!-- FIN: Partie principale de la page -->

        <!-- DEBUT: pied de page fixe -->
        <?php include("sections/footer.php"); ?>
        <!-- FIN: pied de page -->

        <!-- DEBUT: modal de login -->
        <?php include("modals/login.php"); ?>
        <!-- FIN: modal de login -->

        <!-- DEBUT: modal d'inscription -->
        <?php include("modals/signin.php"); ?>
        <!-- FIN: modal d'inscription -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Ajouter le code PHP pour gérer le login invalide -->
<?php
    if (isset($login) and $login == false) {
        $modal = '#loginModal';

    } elseif (isset($signin) and $signin == false) {
        $modal = '#signinModal';
    }

    if (isset($modal)) {
        echo '<script type="text/javascript">
                $(window).load(function(){
                    $(\'' . $modal . '\').modal(\'show\');
                });
            </script>';
   } 
?>
    </body>
</html>

